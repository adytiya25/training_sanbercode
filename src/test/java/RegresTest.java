
import io.restassured.RestAssured;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

public class RegresTest {
    @BeforeClass
    public void setup(){
        RestAssured.baseURI="https://reqres.in/";
    }
    @Test
    public void getSingleUser(){
        given()
                .when()
                .get("/api/users/2")
                .then()
                .body("data.id",equalTo(2))
                .body("data.email",equalTo("janet.weaver@reqres.in"))
                .body("data.first_name",equalTo("Janet"))
                .body("data.last_name",equalTo("Weaver"))
                .statusCode(200)
                .log().all();
    }
    @Test
    public void getListUser(){
        given()
                .when()
                .get("/api/users?page=2")
                .then()
                .body(containsString("rachel.howell@reqres.in"))
                .statusCode(200)
                .log().all();
    }
    @Test
    public void createUser(){
        Map<String,String> user = new HashMap<>();
        user.put("name", "morpheus");
        user.put("job", "leader");

        given()
                .contentType("application/json")
                .body(user)
                .when()
                .post("/api/users")
                .then()
                .body("name",equalTo("morpheus"))
                .body("job",equalTo("leader"))
                .body("id",notNullValue())
                .body("createdAt",notNullValue())
                .body("updatedAt",nullValue())
                .statusCode(201)
                .log().all();
    }
    @Test
    public void deleteUser(){
        given()
                .when()
                .delete("/api/users/2")
                .then()
                .statusCode(204)
                .log().all();
    }
}
